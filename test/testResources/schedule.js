export default {
    "resourceType": "Schedule",
    "id": "3242828",
    "meta": {
        "versionId": "1",
        "lastUpdated": "2018-05-04T06:51:52.064+00:00",
        "tag": [
            {
              "system": "http://projectcrucible.org",
              "code": "testdata"
            }
        ]
    },
    "language": "fr-FR",
    "text": {
      "status": "generated",
      "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\">eFkxF/p3q1vqDc5eoz2nvg==</div>"
    },
    "identifier": [
        {
            "use": "secondary",
            "type": {
                "coding": [
                    {
                        "system": "http://hl7.org/fhir/v2/0203",
                        "code": "MCN",
                        "display": "Microchip Number",
                        "userSelected": false
                    }
                ],
                "text": "g2HLuJ7Oh8onrgvMXdbPEw=="
            },
            "system": "http://projectcrucible.org/W0tHPmYqGBnD+PH2LmdrEg==",
            "value": "RSeAQ4fjuEv9R5TItKEw5Q==",
            "period": {
                "start": "2018-05-04T12:18:50.653Z",
                "end": "2018-05-04T12:18:50.653Z"
            },
            "assigner": {
                "identifier": {
                    "use": "temp",
                    "system": "http://projectcrucible.org/TX64hOHS/0DH6rRPt22QoQ==",
                    "value": "2v+Cq+ni/PF5qs+TZLGwYg=="
                },
                "display": "Organization JAAadAYpbfuNjDJl1vSHpA=="
            }
        }
    ],
    "active": false,
    "serviceCategory": {
        "coding": [
            {
                "system": "http://hl7.org/fhir/service-category",
                "code": "28",
                "display": "Specialist Obstetrics & Gynaecology",
                "userSelected": false
            }
        ],
        "text": "YvtJOfAGKCcIvLHCq+zrgg=="
    },
    "serviceType": [
        {
            "coding": [
                {
                    "system": "http://hl7.org/fhir/service-type",
                    "code": "190",
                    "version": "0.0.1",
                    "display": "Paediatric Cardiology",
                    "userSelected": false
                }
            ],
            "text": "aCh0YWpdqsC79Aw2Ssz0hQ=="
        }
    ],
    "specialty": [
        {
            "coding": [
                {
                    "system": "http://snomed.info/sct",
                    "code": "408472002",
                    "display": "Hepatology",
                    "userSelected": false
                }
            ],
            "text": "WUOSWlbj8ImnTxs0vrwsYA=="
        }
    ],
    "actor": [
        {
            "identifier": {
                "use": "temp",
                "type": {
                    "text": "INA0hsoXKf9JML539Zcu4Q=="
                },
                "system": "http://projectcrucible.org/HNGoimW0Cg7CqRrutE9vkg==",
                "value": "WA+mojm9uF7a1seC5pegBw==",
                "period": {
                    "start": "2018-05-04T12:18:50.669Z",
                    "end": "2018-05-04T12:18:50.669Z"
                },
                "assigner": {
                    "display": "Organization H1szmBgYekrsd/zVfTDcqw=="
                }
            },
            "display": "RelatedPerson iAj/x5kgqQ26cqCJinAGdg=="
        }
    ],
    "planningHorizon": {
        "start": "2018-05-04T12:18:50.670Z",
        "end": "2018-05-04T12:18:50.670Z"
    },
    "comment": "Bkr0mXnSxYxF49cG3C243g=="
}